
package com.azuril.ihm;

import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.prefs.Preferences;

import org.ini4j.Ini;
import org.ini4j.IniPreferences;

import com.azuril.crud.CrudGenerator;
import com.azuril.util.LogAzuril;

public class JavaCrudGenerator
{

	public static String getValue(Preferences section, String key)
	{

		String value = section.get(key, "");

		String r = value.replaceAll("\"", "");

		return r;
	}

	public static void main(String[] args) throws SecurityException,
			IOException
	{

		String programName = JavaCrudGenerator.class.getCanonicalName();

		System.out.println(" programName : ");
		System.out.println(programName);

		Level txtLvl = Level.INFO;

		try
		{

			File file_current = new File(JavaCrudGenerator.class
					.getProtectionDomain().getCodeSource().getLocation()
					.toURI().getPath()).getParentFile();

			Ini ini = new Ini(new File(file_current.getPath(), "javaIni.ini"));
			Preferences pref_ini = new IniPreferences(ini);
			Preferences section = pref_ini.node("crudGenerator");

			String s_level = getValue(section, "level");

			txtLvl = Level.parse(s_level);

		} catch (Exception ex)
		{
			ex.printStackTrace();
			System.out.println(ex.toString());
		}

		LogAzuril logger = new LogAzuril(programName, Level.ALL, txtLvl);

		logger.log(Level.INFO, " debut ");

		StringBuilder sb_url = new StringBuilder();

		try
		{

			File file_current = new File(JavaCrudGenerator.class
					.getProtectionDomain().getCodeSource().getLocation()
					.toURI().getPath());

			file_current = file_current.getParentFile();

			String s_baseDirectory = file_current.getPath();

			logger.log(Level.INFO, " current Path : ");
			logger.log(Level.INFO, s_baseDirectory);

			File file_ini = new File(s_baseDirectory, "javaIni.ini");

			String iniFilePath = file_ini.getPath();

			logger.log(Level.INFO, " iniFilePath : ");
			logger.log(Level.INFO, iniFilePath);

			Ini ini = new Ini(file_ini);

			Preferences pref_ini = new IniPreferences(ini);

			Preferences section = pref_ini.node("crudGenerator");
			String s_pathFolderCode = getValue(section, "folderOut");

			logger.log(Level.INFO, " s_pathFolderCode : ");
			logger.log(Level.INFO, s_pathFolderCode);

			File folderCode = new File(s_pathFolderCode);

			String s_connectionString = getValue(section, "connectionString");

			logger.log(Level.INFO, " s_connectionString : ");
			logger.log(Level.INFO, s_connectionString);

			CrudGenerator gen = new CrudGenerator(s_connectionString,
					folderCode, logger);

			sb_url = gen.javaGenerate();

			// AzurilTools.bug();

		} catch (Exception ex)
		{
			ex.printStackTrace();
			String err = ex.toString();
			logger.log(Level.SEVERE, err);
		}

		logger.log(Level.INFO, " sb_url : ");
		logger.log(Level.FINE, sb_url.toString());

		logger.log(Level.INFO, " fin ");

	}

}
