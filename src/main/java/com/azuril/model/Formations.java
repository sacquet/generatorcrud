
package com.azuril.model;

public class Formations
{

	private int id;

	private String libelle;

	private String description;

	private String debut;

	private String fin;

	public Formations()
	{

	}

	public int getId()
	{

		return this.id;
	}

	public void setId(int id)
	{

		this.id = id;
	}

	public String getLibelle()
	{

		return this.libelle;
	}

	public void setLibelle(String libelle)
	{

		this.libelle = libelle;
	}

	public String getDescription()
	{

		return this.description;
	}

	public void setDescription(String description)
	{

		this.description = description;
	}

	public String getDebut()
	{

		return this.debut;
	}

	public void setDebut(String debut)
	{

		this.debut = debut;
	}

	public String getFin()
	{

		return this.fin;
	}

	public void setFin(String fin)
	{

		this.fin = fin;
	}

	@Override
	public String toString()
	{

		StringBuilder sb = new StringBuilder();

		sb.append(this.id);
		sb.append(";");
		sb.append(this.libelle);
		sb.append(";");
		sb.append(this.description);
		sb.append(";");
		sb.append(this.debut);
		sb.append(";");
		sb.append(this.fin);
		sb.append(";");

		return sb.toString();
	}

}
