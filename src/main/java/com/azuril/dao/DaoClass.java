
package com.azuril.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Logger;

import com.azuril.util.MonLogger;

public class DaoClass
{

	@SuppressWarnings("unused")
	private static final Logger monLogger = MonLogger.getLogger();

	private static String _cs;

	@SuppressWarnings("unused")
	private final static String _driver = "com.microsoft.sqlserver.jdbc.SQLServerDriver";

	@SuppressWarnings("unused")
	private static boolean _isDriverInit = false;

	private static void initDriver()
	{

		try
		{
			com.microsoft.sqlserver.jdbc.SQLServerDriver.class.newInstance();

		} catch (Exception e)
		{
			e.printStackTrace();
		}
	}

//	private static void initDriver_V1()
//	{
//
//		try
//		{
//
//			com.microsoft.sqlserver.jdbc.SQLServerDriver.class.newInstance();
//
//			// Class.forName(_driver).newInstance();
//
//			String cs = "jdbc:sqlserver://localhost;user=sa;password=Pa$$w0rd;databaseName=TPJavaEE_GestionFormations;";
//
//			try (Connection cnx = DriverManager.getConnection(cs))
//			{
//				String csv = AzurilSql.getCsvFromSql(cnx,
//						" SELECT TOP 1 * FROM formations ");
//
//				System.out.println(csv);
//			}
//
//			_isDriverInit = true;
//
//		} catch (Exception ex)
//		{
//			ex.printStackTrace();
//			monLogger.severe(ex.toString());
//		}
//
//	}

	private DaoClass()
	{

		initDriver();
	}

	public DaoClass(String cs) throws SQLException
	{

		this();
		DaoClass._cs = cs;
	}

	public Connection getConnection() throws SQLException
	{

		initDriver();

		Connection cnx = DriverManager.getConnection(_cs);

		return cnx;
	}
}
