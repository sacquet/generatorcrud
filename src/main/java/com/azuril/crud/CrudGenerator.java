
package com.azuril.crud;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.azuril.dao.DaoClass;
import com.azuril.util.AzurilTools;
import com.azuril.util.DaoException;

public class CrudGenerator
{

	private String _connectionString;

	private File _pathFolderCode;
	
	private Logger _logger;

	public CrudGenerator(String connectionString, File pathFolderCode , Logger logger)
	{

		_connectionString = connectionString;

		_pathFolderCode = pathFolderCode;
		AzurilTools.createFolder(_pathFolderCode);
		
		_logger = logger;
	}

	public StringBuilder javaGenerate() throws SQLException, DaoException,
			IOException
	{

		// / init conf

		File pathFolder = new File(_pathFolderCode, "java");

		String racineHtml = pathFolder.getAbsolutePath();

		_logger.log(Level.INFO, " racineHtml : ");
		_logger.log(Level.INFO, racineHtml);

		if (AzurilTools.createFolder(pathFolder))
		{
			pathFolder = new File(_pathFolderCode + "/java/");
		}

		_logger.log(Level.INFO, " Path du dossier de sortie des fichiers java : ");
		_logger.log(Level.INFO, pathFolder.getPath());

		// / start

		_logger.log(Level.INFO, "====================");
		_logger.log(Level.INFO, " java generation start ");
		_logger.log(Level.INFO, "====================");

		StringBuilder sb = new StringBuilder();

		_logger.log(Level.INFO, " phase 0 : on ce connecte a la BDD ");
		
		Connection cnx = null;

		
		cnx = new DaoClass(_connectionString).getConnection();

		// / phase 1 : on recuperre les info des colones
		_logger.log(Level.INFO, " phase 1 : on recuperre les info des colones ");

		ArrayList<Column> ls_col = CrudDao.getAllColumn(cnx);

		// / phase 2 : on cree les objet Table
		_logger.log(Level.INFO, " phase 2 : on cree les objet Table ");

		ArrayList<Table> l_table = CrudDao.toTable(ls_col);

		// / phase 3 : on genere la BO

		CrudToJava crudJava = new CrudToJava();

		ArrayList<String> ls_Model = crudJava.getModel(l_table);

		// / phase 4 : on ecrit le Model et on print des lien

		ArrayList<String> ls_uri = new ArrayList<String>();

		for (int i = 0; i < ls_Model.size(); i++)
		{
			Path pathFile = Paths.get(pathFolder.getPath(), l_table.get(i)
						.getTableName() + ".java");

			AzurilTools.writeAllLines_V2(ls_Model.get(i), pathFile);

			ls_uri.add(racineHtml + l_table.get(i).getTableName() + ".java");
		}

		// / phase 5 : on genere la Dao

		ArrayList<String> ls_Dao = crudJava.getDao(l_table);

		// / phase 6 : on ecrit la Dao et on print des lien

		for (int i = 0; i < ls_Dao.size(); i++)
		{
			Path pathFile = Paths.get(pathFolder.getPath(), l_table.get(i)
						.getTableName() + "Dao.java");

			AzurilTools.writeAllLines_V2(ls_Dao.get(i), pathFile);

			ls_uri.add(racineHtml + l_table.get(i).getTableName()
						+ "Dao.java");
		}

		// / phase 7 : print debug

		CrudToHtml.printLink(sb, ls_uri);

		// CrudToHtml.printTable(sb, l_table);
		// CrudToHtml.printCol(sb, ls_col);

		sb.append(" <hr /> ");

		_logger.log(Level.INFO, "====================");
		_logger.log(Level.INFO, " java generation end ");
		_logger.log(Level.INFO, "====================");

		return sb;
	}

}
