
package com.azuril.crud;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Set;

import com.azuril.util.AzurilTools;
import com.azuril.util.DaoException;
import com.azuril.util.AzurilSql;

public class CrudDao
{

	public static ArrayList<Table> toTable(ArrayList<Column> ls_col)
	{

		System.out.println(" ls_col.size() : ");
		System.out.println(ls_col.size());

		ArrayList<String> ls_table = new ArrayList<String>();

		for (Column column : ls_col)
		{
			ls_table.add(column.get_TABLE_NAME());
		}

		Set<String> set_table = AzurilTools.distinct(ls_table);

		ArrayList<Table> l_table = new ArrayList<Table>();

		for (String s_table : set_table)
		{
			l_table.add(new Table(s_table));
		}

		System.out.println(" l_table.size() : ");
		System.out.println(l_table.size());

		for (Column column : ls_col)
		{

			String tableName = column.get_TABLE_NAME().trim().toUpperCase();

			Table tableAdd = null;

			for (int i = 0; i < l_table.size(); i++)
			{
				String localTableName = l_table.get(i).getTableName().trim()
						.toUpperCase();

				if (localTableName.equals(tableName))
				{
					tableAdd = l_table.get(i);
					break;
				}
			}

			if (tableAdd != null)
			{
				tableAdd.add(column);
			} else
			{
				System.out.println(" pas de table pour la colone : ");
				System.out.println(column.toString());
			}

		}

		return l_table;
	}

	
	public static ArrayList<String> getTableList(Connection cnx)
			throws SQLException
	{

		ArrayList<String> ls_table = new ArrayList<String>();

		String sql_tables = " SELECT TABLE_NAME "
				+ " FROM information_schema.tables "
				+ " WHERE TABLE_TYPE IN ('BASE TABLE','VIEW') ";

		ls_table = AzurilSql.getListCsvFromSql(cnx, sql_tables);

		return ls_table;
	}

	public static ArrayList<String> getColumnFromTable(Connection cnx)
			throws SQLException
	{

		ArrayList<String> ls_table = new ArrayList<String>();

		String sql_tables = " SELECT TABLE_NAME "
				+ " FROM information_schema.tables "
				+ " WHERE TABLE_TYPE IN ('BASE TABLE','VIEW') ";

		ls_table = AzurilSql.getListCsvFromSql(cnx, sql_tables);

		return ls_table;
	}

	public static ArrayList<Column> getAllColumn(Connection cnx)
			throws SQLException, DaoException
	{

		ArrayList<Column> ls_table = new ArrayList<Column>();

		String sql_allColumn = " " + 
				" SELECT " + 
				" t_col.TABLE_NAME ,COLUMN_NAME , DATA_TYPE, TABLE_TYPE " + 
				" FROM INFORMATION_SCHEMA.COLUMNS as t_col " + 
				" JOIN " + 
				" ( " + 
				" SELECT TABLE_NAME , TABLE_TYPE " + 
				" FROM information_schema.tables " + 
				" WHERE TABLE_TYPE IN ('BASE TABLE','VIEW') " + 
				" ) AS tt_tables " + 
				" ON t_col.TABLE_NAME = tt_tables.TABLE_NAME " + 
				" ";

		ResultSet rs = AzurilSql.getResultSetFromSql(cnx, sql_allColumn);
		
		while (rs.next())
		{
			ls_table.add(itemBuilder(rs));
		}

		return ls_table;
	}

	public static Column itemBuilder(ResultSet rs) throws DaoException
	{

		Column column = null;

		try
		{

			String TABLE_NAME = rs.getString("TABLE_NAME");
			String COLUMN_NAME = rs.getString("COLUMN_NAME");
			String DATA_TYPE = rs.getString("DATA_TYPE");
			String TABLE_TYPE = rs.getString("TABLE_TYPE");

			column = new Column(TABLE_NAME, COLUMN_NAME, DATA_TYPE, TABLE_TYPE);

		} catch (SQLException e)
		{

			String err = " SQLException dans PersonneDao itemBuilder : "
					+ e.getMessage();

			e.printStackTrace();

			throw new DaoException(err, e);

		}

		return column;
	}

}
