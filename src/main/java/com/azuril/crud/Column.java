
package com.azuril.crud;

public class Column
{

	private String _TABLE_NAME;

	private String _COLUMN_NAME;

	private String _DATA_TYPE;

	private String _TABLE_TYPE;

	public Column(String TABLE_NAME, String COLUMN_NAME, String DATA_TYPE, String TABLE_TYPE)
	{
		this._TABLE_NAME = TABLE_NAME;
		this._COLUMN_NAME = COLUMN_NAME;
		this._DATA_TYPE = DATA_TYPE;
		this._TABLE_TYPE = TABLE_TYPE;
	}

	public String get_TABLE_NAME()
	{

		return _TABLE_NAME;
	}

	public void set_TABLE_NAME(String _TABLE_NAME)
	{

		this._TABLE_NAME = _TABLE_NAME;
	}

	public String get_COLUMN_NAME()
	{

		return _COLUMN_NAME;
	}

	public void set_COLUMN_NAME(String _COLUMN_NAME)
	{

		this._COLUMN_NAME = _COLUMN_NAME;
	}

	public String get_DATA_TYPE()
	{

		return _DATA_TYPE;
	}

	public void set_DATA_TYPE(String _DATA_TYPE)
	{

		this._DATA_TYPE = _DATA_TYPE;
	}

	public String get_TABLE_TYPE()
	{

		return _TABLE_TYPE;
	}

	public void set_TABLE_TYPE(String _TABLE_TYPE)
	{

		this._TABLE_TYPE = _TABLE_TYPE;
	}

	@Override
	public String toString()
	{

		StringBuilder builder = new StringBuilder();
		builder.append("Column [_TABLE_NAME=");
		builder.append(_TABLE_NAME);
		builder.append(", _COLUMN_NAME=");
		builder.append(_COLUMN_NAME);
		builder.append(", _DATA_TYPE=");
		builder.append(_DATA_TYPE);
		builder.append(", _TABLE_TYPE=");
		builder.append(_TABLE_TYPE);
		builder.append("]");
		return builder.toString();
	}

}
