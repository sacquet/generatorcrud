
package com.azuril.crud;

import java.util.ArrayList;
import java.util.Set;

import com.azuril.util.AzurilTools;

public class CrudToHtml
{

	public static void printTable(StringBuilder sb, ArrayList<Table> l_table)
	{

		sb.append(" <hr /> ");

		sb.append(" les Tables : ");

		sb.append(" <br /> ");
		sb.append(" <br /> ");

		for (Table table : l_table)
		{
			sb.append(table.toHtml());
		}

		sb.append(" <br /> ");
		sb.append(" <br /> ");

		sb.append(" <hr /> ");
	}

	public static void printCol(StringBuilder sb, ArrayList<Column> ls_col)
	{

		ArrayList<String> ls_table = new ArrayList<String>();

		for (Column column : ls_col)
		{
			ls_table.add(column.get_TABLE_NAME());
		}

		Set<String> set_table = AzurilTools.distinct(ls_table);

		sb.append(" <hr /> ");

		sb.append(" le Nom des Tables : ");
		sb.append(" <br /> ");
		sb.append(" <br /> ");

		for (String s_table : set_table)
		{
			sb.append(s_table);
			sb.append(" <br /> ");
			sb.append(" <br /> ");
		}

		sb.append(" <hr /> ");

		sb.append(" les Colonnes : ");
		sb.append(" <br /> ");
		sb.append(" <br /> ");

		for (Column column : ls_col)
		{
			sb.append(column.toString());
			sb.append(" <br /> ");
			sb.append(" <br /> ");
		}

		sb.append(" <hr /> ");
	}

	public static void printLink(StringBuilder sb, ArrayList<String> ls_uri)
	{

		sb.append(" <hr /> ");

		sb.append(" <p> ");

		for (String uri : ls_uri)
		{
			sb.append("<a href=\"");
			sb.append(uri);
			sb.append(" \"> ");
			sb.append(uri);
			sb.append("</a>");
			sb.append("<br />");
		}

		sb.append(" </p> ");

		sb.append(" <hr /> ");
	}

}
