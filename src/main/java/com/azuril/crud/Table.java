
package com.azuril.crud;

import java.util.ArrayList;

public class Table
{

	private String tableName;

	private ArrayList<Column> ls_col;

	public Table(String tableName)
	{
		this.ls_col = new ArrayList<Column>();
		
		tableName = tableName.substring(0,1).toUpperCase()+tableName.substring(1);
		
		this.tableName = tableName;
	}

	public String getTableName()
	{

		return tableName;
	}

	public void setTableName(String tableName)
	{

		this.tableName = tableName;
	}

	public ArrayList<Column> getLs_Col()
	{

		return ls_col;
	}

	public void setLs_col(ArrayList<Column> ls_col)
	{

		this.ls_col = ls_col;
	}

	public void add(Column col)
	{

		this.ls_col.add(col);
	}

	@Override
	public String toString()
	{

		StringBuilder builder = new StringBuilder();
		builder.append("Table [tableName=");
		builder.append(tableName);
		builder.append(", ls_col=");
		builder.append(ls_col.toString());
		builder.append("]");
		return builder.toString();
	}


	
	public String toHtml()
	{

		StringBuilder builder = new StringBuilder();
		
		builder.append("<br />");
		builder.append("<br />");
		
		
		builder.append("Table [tableName=");
		builder.append(tableName);
		builder.append(", ls_col=");
		builder.append("<br />");
		
		for (Column column : ls_col)
		{
			builder.append(column.toString());
			builder.append("<br />");
		}
		builder.append("]");

		builder.append("<br />");
		builder.append("<br />");
		
		return builder.toString();
	}

	
	
}
