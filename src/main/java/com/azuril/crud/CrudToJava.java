
package com.azuril.crud;

import java.util.ArrayList;

public class CrudToJava
{

	public final static String GET_CNX = " new DaoClass().getConnection() ";

	public CrudToJava()
	{

	}

	private void log(Exception ex)
	{

	}

	private String up(String s)
	{

		return s.substring(0, 1).toUpperCase() + s.substring(1);
	}

	private void appendType(StringBuilder sb, Column col)
	{

		if ("int".equals(col.get_DATA_TYPE()))
		{
			sb.append("int");

		} else
		{
			sb.append("String");
		}
	}

	private void appendTypeUp(StringBuilder sb, Column col)
	{

		if ("int".equals(col.get_DATA_TYPE()))
		{
			sb.append("Int");

		} else
		{
			sb.append("String");
		}
	}

	private String getModel(Table table)
	{

		StringBuilder sb = new StringBuilder();

		sb.append("\n");
		sb.append("package com.azuril.model; \n");
		sb.append("\n");
		sb.append("public class ");
		sb.append(table.getTableName());
		sb.append(" \n");
		sb.append("{ \n");

		ArrayList<Column> columns = table.getLs_Col();

		// / atribut

		for (Column col : columns)
		{
			sb.append("\n");
			sb.append(" private ");
			appendType(sb, col);
			sb.append(" ");
			sb.append(col.get_COLUMN_NAME());
			sb.append(" ; \n");
		}

		// / constructor empty

		sb.append("\n");
		sb.append("\n");

		sb.append(" public ");
		sb.append(table.getTableName());
		sb.append("()");
		sb.append("\n");
		sb.append("{");
		sb.append("\n");
		sb.append("\n");
		sb.append("}");

		// / getter setter

		for (Column col : columns)
		{

			String varName = col.get_COLUMN_NAME();
			varName = up(varName);

			// / getter

			sb.append("\n");
			sb.append("\n");

			sb.append(" public ");

			appendType(sb, col);

			sb.append(" get");
			sb.append(varName);
			sb.append("()");
			sb.append("\n");
			sb.append("{\n");
			sb.append(" return this.");
			sb.append(col.get_COLUMN_NAME());
			sb.append(" ; \n");
			sb.append("}\n");

			sb.append("\n");

			// / setter

			sb.append(" public void set");
			sb.append(varName);
			sb.append("(");

			appendType(sb, col);

			sb.append(" ");
			
			sb.append(col.get_COLUMN_NAME());

			sb.append(")\n");
			sb.append("{\n");

			sb.append("this.");
			sb.append(col.get_COLUMN_NAME());
			sb.append(" = ");
			sb.append(col.get_COLUMN_NAME());

			sb.append(" ; \n");
			sb.append("} \n");

		}

		sb.append("\n");
		sb.append("\n");

		sb.append("@Override \n");
		sb.append("public String toString() \n");
		sb.append("{ \n");
		sb.append(" StringBuilder sb = new StringBuilder(); \n");
		sb.append(" \n");

		for (Column col : columns)
		{
			sb.append(" sb.append(this.");
			sb.append(col.get_COLUMN_NAME());
			sb.append(");\n");

			sb.append(" sb.append(\";\");\n");
		}

		sb.append("\n");

		sb.append(" return sb.toString(); \n");
		sb.append("} \n");

		sb.append("\n");
		sb.append("\n");

		// / end

		sb.append("\n");
		sb.append("\n");
		sb.append("\n");
		sb.append("} \n");
		sb.append("\n");

		return sb.toString();
	}

	public ArrayList<String> getModel(ArrayList<Table> l_table)
	{

		ArrayList<String> ls = new ArrayList<String>();

		for (Table table : l_table)
		{

			String s_java = "";

			try
			{
				s_java = getModel(table);

			} catch (Exception ex)
			{
				log(ex);
				String err = ex.toString();
				s_java += " /* " + err + " */ ";
			}

			ls.add(s_java);

		}

		return ls;
	}

	private String getDao(Table table)
	{

		ArrayList<Column> columns = table.getLs_Col();

		StringBuilder sb = new StringBuilder();

		String r = "r";

		String id_name = columns.get(0).get_COLUMN_NAME();

		// / ====================
		// / start
		// / ====================

		sb.append("\n");
		sb.append("package com.azuril.dao; \n");

		sb.append("\n");
		sb.append("import java.sql.*; \n");
		sb.append("import java.util.*; \n");
		sb.append("\n");
		sb.append("import com.azuril.util.*; \n");
		sb.append("import com.azuril.model.*; \n");
		sb.append("\n");

		sb.append("\n");
		sb.append("public class ");
		sb.append(table.getTableName());
		sb.append("Dao ");
		sb.append(" \n");
		sb.append("{ \n");
		sb.append(" \n");

		// / SQL_SELECT

		sb.append(" \n");
		sb.append("public final static String ");
		sb.append(" SQL_SELECT_ALL = \" SELECT * FROM ");
		sb.append(table.getTableName());
		sb.append(" \"; ");
		sb.append(" \n");

		// / SQL_SELECT_ONE

		sb.append(" \n");
		sb.append("public final static String ");
		sb.append(" SQL_SELECT_ONE = \" SELECT * FROM ");
		sb.append(table.getTableName());
		sb.append("  WHERE ");
		sb.append(columns.get(0).get_COLUMN_NAME());
		sb.append(" = ? ");
		sb.append(" \"; ");
		sb.append(" \n");

		// / SQL_DELETE_ONE

		sb.append(" \n");
		sb.append("public final static String ");
		sb.append(" SQL_DELETE_ONE = \" DELETE FROM ");
		sb.append(table.getTableName());
		sb.append("  WHERE ");
		sb.append(columns.get(0).get_COLUMN_NAME());
		sb.append(" = ? ");
		sb.append(" \"; ");
		sb.append(" \n");

		// / public final static String SQL_DELETE_ONE =
		// / " DELETE FROM formations WHERE id = ?  ";

		// / SQL_ADD_ONE

		sb.append(" \n");
		sb.append("public final static String ");
		sb.append(" SQL_ADD_ONE = \n ");
		sb.append(" \" INSERT INTO ");
		sb.append(table.getTableName());
		sb.append(" ( ");
		sb.append(columns.get(1).get_COLUMN_NAME());

		for (int i = 2; i < columns.size(); i++)
		{
			sb.append(",");
			sb.append(columns.get(i).get_COLUMN_NAME());
		}

		sb.append(" ) ");
		sb.append(" VALUES ( ?");

		for (int i = 2; i < columns.size(); i++)
		{
			sb.append(",?");
		}

		sb.append(" ) \"; ");

		sb.append("\n");
		sb.append("\n");

		// public final static String SQL_ADD_ONE = " INSERT INTO formations "
		// + " ( libelle,description,debut,fin ) "
		// + " VALUES ( ?,?,?,? ) ";

		// / SQL_UPDATE_ONE

		sb.append(" \n");
		sb.append("public final static String ");
		sb.append(" SQL_UPDATE_ONE = \n ");
		sb.append(" \" UPDATE TABLE ");
		sb.append(table.getTableName());
		sb.append(" SET ");
		sb.append(columns.get(1).get_COLUMN_NAME());
		sb.append(" = ? ");

		for (int i = 2; i < columns.size(); i++)
		{
			sb.append(",");
			sb.append(columns.get(i).get_COLUMN_NAME());
			sb.append(" = ? ");
		}

		sb.append("  WHERE ");
		sb.append(columns.get(0).get_COLUMN_NAME());
		sb.append(" = ? ");

		sb.append(" \"; ");

		sb.append("\n");
		sb.append("\n");

		// public final static String SQL_UPDATE_ONE = " UPDATE formations "
		// + " SET [libelle] = ?,[description] = ?,[debut] = ?,[fin] = ? "
		// + " WHERE id = ? ";

		// / constructor empty

		sb.append("\n");
		sb.append("\n");

		sb.append("public ");
		sb.append(table.getTableName());
		sb.append("Dao");
		sb.append("()");
		sb.append("\n");
		sb.append("{");
		sb.append("\n");
		sb.append("\n");
		sb.append("} \n");
		sb.append("\n");
		sb.append("\n");
		sb.append("\n");

		// / itemBuilder

		sb.append("public static ");
		sb.append(table.getTableName());
		sb.append(" itemBuilder(ResultSet rs) throws SQLException\n");
		sb.append("{ \n");
		sb.append("\n");

		sb.append(" ");
		sb.append(table.getTableName());
		sb.append(" ");
		sb.append(r);
		sb.append(" = new ");
		sb.append(table.getTableName());
		sb.append("(); \n");
		sb.append("\n");

		for (Column col : columns)
		{
			String prop = up(col.get_COLUMN_NAME());

			sb.append(" ");
			sb.append(r);
			sb.append(".set");
			sb.append(prop);
			sb.append("(rs.get");

			appendTypeUp(sb, col);

			sb.append("(\"");
			sb.append(col.get_COLUMN_NAME());
			sb.append("\"));");

			sb.append("\n");
		}

		sb.append("\n");
		sb.append(" return ");
		sb.append(r);
		sb.append("; \n");
		sb.append("} \n");
		sb.append("\n");
		sb.append("\n");

		// / getAll

		sb.append("\n");
		sb.append("public static ArrayList<");
		sb.append(table.getTableName());
		sb.append("> getAll() throws SQLException \n");
		sb.append("{ \n");
		sb.append(" ArrayList<");
		sb.append(table.getTableName());
		sb.append("> ");
		sb.append(r);
		sb.append("  = new ArrayList<");
		sb.append(table.getTableName());
		sb.append(">(); \n");
		sb.append("\n");

		sb.append(" try (Connection cnx = ");
		sb.append(GET_CNX);
		sb.append(" ) \n");
		sb.append(" { \n");
		sb.append("  ResultSet rs = AzurilSql.getResultSetFromSql(cnx, SQL_SELECT_ALL); \n");
		sb.append("  \n");
		sb.append("  if (rs.next()) \n");
		sb.append("  { \n");
		sb.append(r);
		sb.append(".add(itemBuilder(rs));");
		sb.append("   \n");
		sb.append("  } \n");
		sb.append("\n");
		sb.append("return ");
		sb.append(r);
		sb.append(" ; \n");
		sb.append(" } \n");
		sb.append("} \n");
		sb.append("\n");
		sb.append("\n");

		// / getById

		sb.append("\n");
		sb.append("public static ");
		sb.append(table.getTableName());
		sb.append(" getById(");
		appendType(sb, columns.get(0));
		sb.append(" ");
		sb.append(id_name);
		sb.append(") throws SQLException \n");
		sb.append("{ \n");
		sb.append(" ");
		sb.append(table.getTableName());
		sb.append(" ");
		sb.append(r);
		sb.append(" = new ");
		sb.append(table.getTableName());
		sb.append("(); \n");
		sb.append("\n");

		sb.append("String s_id = String.valueOf(");
		sb.append(id_name);
		sb.append("); \n");
		sb.append("\n");
		sb.append("String[] paramValue = \n");
		sb.append(" { \n");
		sb.append("s_id \n");
		sb.append(" }; \n");
		sb.append("\n");

		sb.append(" try (Connection cnx = ");
		sb.append(GET_CNX);
		sb.append(" ) \n");
		sb.append(" { \n");
		sb.append("\n");
		sb.append("  ResultSet rs = AzurilSql.getResultSetFromSql(cnx, SQL_SELECT_ONE, paramValue); \n");
		sb.append("\n");
		sb.append("  rs.next(); \n");
		sb.append(r);
		sb.append(" = itemBuilder(rs); \n");
		sb.append("  } \n");
		sb.append("\n");
		sb.append("return ");
		sb.append(r);
		sb.append(" ; \n");
		sb.append("} \n");
		sb.append("\n");
		sb.append("\n");

		// / add

		sb.append("\n");
		sb.append("public static void add( ");
		sb.append(table.getTableName());
		sb.append(" item) throws SQLException \n");
		sb.append("{ \n");
		sb.append("\n");
		sb.append(" Object[] paramValue = \n");
		sb.append(" { \n");

		sb.append(" item.get");
		sb.append(up(columns.get(1).get_COLUMN_NAME()));
		sb.append("()");

		for (int i = 2; i < columns.size(); i++)
		{
			sb.append(",");
			sb.append(" item.get");
			sb.append(up(columns.get(i).get_COLUMN_NAME()));
			sb.append("()");
		}

		sb.append("\n");
		sb.append(" }; \n");
		sb.append("\n");
		sb.append("\n");
		sb.append(" try (Connection cnx = ");
		sb.append(GET_CNX);
		sb.append(" ) \n");
		sb.append(" { \n");
		sb.append("  AzurilSql.querryCrash(cnx, SQL_ADD_ONE, paramValue); \n");
		sb.append(" } \n");
		sb.append("\n");
		sb.append("} \n");

		// / deleteById

		sb.append("\n");
		sb.append("public static void deleteById( ");
		appendType(sb, columns.get(0));
		sb.append(" ");
		sb.append(id_name);
		sb.append(") throws SQLException \n");
		sb.append("{ \n");
		sb.append("\n");

		sb.append("String s_id = String.valueOf(");
		sb.append(id_name);
		sb.append("); \n");
		sb.append("\n");
		sb.append("String[] paramValue = \n");
		sb.append(" { \n");
		sb.append("s_id \n");
		sb.append(" }; \n");
		sb.append("\n");

		sb.append("\n");
		sb.append(" try (Connection cnx = ");
		sb.append(GET_CNX);
		sb.append(" ) \n");
		sb.append(" { \n");
		sb.append("  AzurilSql.querryCrash(cnx, SQL_DELETE_ONE, paramValue); \n");
		sb.append(" } \n");
		sb.append("\n");
		sb.append("} \n");

		// / update

		sb.append("\n");
		sb.append("public static void update( ");
		sb.append(table.getTableName());
		sb.append(" item) throws SQLException \n");
		sb.append("{ \n");
		sb.append("\n");
		sb.append(" Object[] paramValue = \n");
		sb.append(" { \n");

		sb.append(" item.get");
		sb.append(up(columns.get(0).get_COLUMN_NAME()));
		sb.append("()");

		for (int i = 1; i < columns.size(); i++)
		{
			sb.append(",");
			sb.append(" item.get");
			sb.append(up(columns.get(i).get_COLUMN_NAME()));
			sb.append("()");
		}

		sb.append("\n");
		sb.append(" }; \n");
		sb.append("\n");
		sb.append("\n");
		sb.append(" try (Connection cnx = ");
		sb.append(GET_CNX);
		sb.append(" ) \n");
		sb.append(" { \n");
		sb.append("  AzurilSql.querryCrash(cnx, SQL_UPDATE_ONE, paramValue); \n");
		sb.append(" } \n");
		sb.append("\n");
		sb.append("} \n");

		// / ====================
		// / end
		// / ====================

		sb.append("\n");
		sb.append("\n");
		sb.append("\n");
		sb.append("} \n");
		sb.append("\n");

		return sb.toString();
	}

	public ArrayList<String> getDao(ArrayList<Table> l_table)
	{

		ArrayList<String> ls = new ArrayList<String>();

		for (Table table : l_table)
		{

			String s_java = "";

			try
			{
				s_java = getDao(table);

			} catch (Exception ex)
			{
				log(ex);
				String err = ex.toString();
				s_java += " /* " + err + " */ ";
			}

			ls.add(s_java);

		}

		return ls;
	}

}
