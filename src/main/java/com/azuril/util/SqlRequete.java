package com.azuril.util;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

public class SqlRequete
{

	private String tableName = "";
	private String[] colonnes;
	private String[] lignes;
	private ArrayList<String> paramValues;

	private String defaultValue = "#NA";

	private boolean isEscape = false;

	private Logger monLogger;
	private int nbPremierreColonne;

	public String getDefaultValue()
	{
		return defaultValue;
	}

	public void setDefaultValue(String defaultValue)
	{
		this.defaultValue = defaultValue;
	}

	public boolean getIs_Escape()
	{
		return isEscape;
	}

	public void setIs_Escape(boolean isEscape)
	{
		this.isEscape = isEscape;
	}

	public SqlRequete(Logger logger, String tableName, String[] colonnes,
			String[] lignes)
	{

		this.monLogger = logger;
		this.tableName = tableName;
		this.colonnes = colonnes;
		this.lignes = lignes;

	}

	public SqlRequete(Logger logger, String tableName, String[] lignes,
			Connection cnx, int premierreColonne) throws SQLException
	{

		this.monLogger = logger;
		this.tableName = tableName;
		this.lignes = lignes;
		this.nbPremierreColonne = premierreColonne;

		String sql_getCol = "SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS  WHERE TABLE_NAME = '"
				+ tableName + "'";

		ArrayList<String> ls = AzurilSql.getListCsvFromSql(cnx, sql_getCol);

		this.colonnes = new AzurilTools<String>().getTableStringFromList(ls);

	}

	public SqlRequete(Logger logger, String tableName, String[] lignes,
			Connection cnx) throws SQLException
	{
		this(logger, tableName, lignes, cnx, 0);
	}

	private String getEntete()
	{
		StringBuilder entete = new StringBuilder();
		entete.append("INSERT INTO " + this.tableName + " ( ");

		entete.append(colonnes[nbPremierreColonne]);

		int i01Max = colonnes.length;

		for (int i01 = nbPremierreColonne + 1; i01 < i01Max; i01++)
		{
			entete.append(", " + colonnes[i01]);
		}

		entete.append(" ) VALUES ( ");

		return entete.toString();
	}

	private String[] restructureLine(int nbColonnes, int ligne)
	{
		String[] valeurColonnesAvant = lignes[ligne].split(";");

		String[] valeurColonnesApres = new String[nbColonnes];

		for (int i01 = nbPremierreColonne; i01 < nbColonnes; i01++)
		{

			if (i01 < valeurColonnesAvant.length)
			{

				String setingCellule = "";

				setingCellule = valeurColonnesAvant[i01];

				setingCellule = "'" + setingCellule.trim() + "'";

				if (setingCellule == "'-'")
				{
					setingCellule = "'" + this.getDefaultValue() + "'";
				} else if (setingCellule == "''")
				{
					setingCellule = "'" + this.getDefaultValue() + "'";
				}

				valeurColonnesApres[i01] = "?";

				this.paramValues.add(valeurColonnesAvant[i01]);

			} else
			{
				valeurColonnesApres[i01] = "NULL";
			}

		}

		return valeurColonnesApres;
	}

	public void execInsert(Connection myConnexion) throws SQLException
	{
		// // insere les donnees

		this.paramValues = new ArrayList<String>();

		// // phase 1 : on cree le String entete
		String entete = getEntete();

		// // phase 2 : on cree le 1 du corp :

		int ligneMax = lignes.length;

		for (int ligne = 0; ligne < ligneMax; ligne = ligne + 100)
		{
			StringBuilder sbSql = new StringBuilder();

			// // on cree la tete

			sbSql.append(entete);

			// // on restructure la ligne pour correspondre aux colones ,
			// 1er

			String[] valeurColonnes02 = this.restructureLine(colonnes.length,
					ligne);

			// // on remplis la ligne avec ce que l'on a mis dans le tableau
			// , 1er

			sbSql.append(valeurColonnes02[nbPremierreColonne].trim());

			for (int i02 = nbPremierreColonne + 1; i02 < valeurColonnes02.length; i02++)
			{
				sbSql.append(", " + valeurColonnes02[i02].trim());
			}

			for (int ligne02 = ligne + 1; (ligne02 < (ligne + 100))
					&& (ligne02 < ligneMax); ligne02++)
			{

				// / on restructure la ligne pour correspondre aux colones ,
				// suivant

				String[] valeurColonnes04 = restructureLine(colonnes.length,
						ligne02);

				// // on remplis la ligne avec ce que l'on a mis dans le
				// tableau , suivant

				sbSql.append("), ("); // // on separe les lignes

				sbSql.append(valeurColonnes04[nbPremierreColonne].trim()); // //
																			// on
																			// commence
				// la ligne

				for (int i02 = nbPremierreColonne + 1; i02 < valeurColonnes04.length; i02++)
				{
					sbSql.append(", " + valeurColonnes04[i02].trim());

					// / on remplis la ligne avec ce que l'on a mis dans le
					// tableau , suivant

				}

			}

			sbSql.append(");");

			this.printLn(" ligneMax : " + ligneMax + " ligne : " + ligne + " ");

			AzurilSql.querryCrash(myConnexion, sbSql.toString(), paramValues);

		}

	}

	private void printLn(String msg)
	{
		this.monLogger.log(Level.INFO, msg);
	}

}
