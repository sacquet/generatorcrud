
package com.azuril.util;

import java.nio.file.Path;
import java.sql.*;
import java.util.*;
import java.io.*;

import javax.xml.parsers.*;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.*;

// import com.microsoft.sqlserver.jdbc.SQLServerDriver;

/**
 * 
 * @author Felix ROBICHON
 *
 *         cree le 2016-01-25
 * 
 *         modifier le 2016-03-23
 * 
 *         ensemble d'outil Sql
 *
 * @param <T>
 */
public class AzurilSql
{

	// public static void setDriverSql() throws SQLException
	// {
	// DriverManager.registerDriver(new SQLServerDriver());
	// }

	public static Connection getConnectionSql(String url) throws SQLException
	{

		// String url =
		// "jdbc:sqlserver://localhost;user=sa;password=Pa$$w0rd;databaseName=ExempleBdd;";
		// private static final String stringConnection =
		// "jdbc:sqlserver://localhost;user=sa;password=Pa$$w0rd;databaseName=GestionParking;";

		Connection cnx = DriverManager.getConnection(url);
		return cnx;
	}

	public static void querryCrash(Connection cnx, String sql)
			throws SQLException
	{

		CallableStatement command = cnx.prepareCall(sql);
		command.execute();
	}

	public static void querryCrash(Connection cnx, String sql,
			String[] paramValue) throws SQLException
	{

		CallableStatement command = cnx.prepareCall(sql);

		for (int i = 1; i <= paramValue.length; i++)
		{
			command.setString(i, paramValue[i - 1]);
		}

		command.execute();

	}

	public static void querryCrash(Connection cnx, String sql,
			Object[] paramValue) throws SQLException
	{

		CallableStatement command = cnx.prepareCall(sql);

		for (int i = 1; i <= paramValue.length; i++)
		{
			String s = String.valueOf(paramValue[i - 1]);

			command.setString(i, s);
		}

		command.execute();

	}

	public static void querryCrashStoredProcedure(Connection cnx,
			String procedureName, String[] paramValue) throws SQLException
	{

		StringBuilder sb_sql = new StringBuilder();

		sb_sql.append("{ call ");
		sb_sql.append(procedureName);
		sb_sql.append("(?");

		for (int i = 1; i < paramValue.length; i++)
		{
			sb_sql.append(",?");
		}

		sb_sql.append(") }");

		CallableStatement command = cnx.prepareCall(sb_sql.toString());

		for (int i = 1; i <= paramValue.length; i++)
		{
			command.setString(i, paramValue[i - 1]);
		}

		command.execute();

	}

	public static ResultSet getResultSetFromSql(Connection cnx, String sql)
			throws SQLException
	{

		return cnx.createStatement().executeQuery(sql);
	}

	public static ResultSet getResultSetFromSql(Connection cnx, String sql,
			String[] paramValue) throws SQLException
	{

		PreparedStatement command = cnx.prepareStatement(sql);

		for (int i = 1; i <= paramValue.length; i++)
		{
			command.setString(i, paramValue[i - 1]);
		}

		command.execute();

		ResultSet rs = command.getResultSet();

		return rs;
	}

	public static ArrayList<String> getListCsvFromSql(ResultSet rs)
			throws SQLException
	{

		ArrayList<String> ls = new ArrayList<String>();

		int columnsNumber = rs.getMetaData().getColumnCount();

		while (rs.next())
		{
			StringBuilder sb = new StringBuilder();
			sb.append(rs.getString(1));

			for (int i = 2; i <= columnsNumber; i++)
			{
				sb.append(";");
				sb.append(rs.getString(i));
			}

			ls.add(sb.toString());
		}

		return ls;
	}

	public static ArrayList<String> getListCsvFromSql(Connection cnx, String sql)
			throws SQLException
	{

		ResultSet rs = cnx.createStatement().executeQuery(sql);
		ArrayList<String> ls = getListCsvFromSql(rs);

		return ls;
	}

	public static String getCsvFromSql(Connection cnx, String sql)
			throws SQLException
	{

		return new AzurilTools<String>().getStringFromList(getListCsvFromSql(
				cnx, sql));
	}

	public static ResultSet getResultSetParam(Connection cnx, String sql,
			String[] paramValue) throws SQLException
	{

		PreparedStatement command = cnx.prepareStatement(sql);

		for (int i = 1; i <= paramValue.length; i++)
		{
			command.setString(i, paramValue[i - 1]);
		}

		command.execute();

		ResultSet rs = command.getResultSet();

		return rs;
	}

	public static ArrayList<String> getListCsvFromResultSet(ResultSet rs,
			String sep1) throws SQLException
	{

		int columnsNumber = rs.getMetaData().getColumnCount();

		ArrayList<String> ls = new ArrayList<String>();

		while (rs.next())
		{
			StringBuilder sb = new StringBuilder();
			sb.append(rs.getString(1));

			for (int i = 2; i <= columnsNumber; i++)
			{
				sb.append(sep1);
				sb.append(rs.getString(i));
			}

			ls.add(sb.toString());
			ls.add(AzurilTools.newLine);
		}

		return ls;
	}

	public static ArrayList<String> getListCsvFromSql(Connection cnx,
			String sql, String[] paramValue, String sep1) throws SQLException
	{

		ResultSet rs = getResultSetParam(cnx, sql, paramValue);
		ArrayList<String> ls = getListCsvFromResultSet(rs, sep1);

		return ls;
	}

	public static ArrayList<String> getListCsvFromResultSet(ResultSet rs)
			throws SQLException
	{

		return getListCsvFromResultSet(rs, ";");
	}

	public static void querryCrash(Connection cnx, String sql,
			ArrayList<String> paramValue) throws SQLException
	{

		String[] ts_paramValue = new AzurilTools<String>()
				.getTableStringFromList(paramValue);

		AzurilSql.querryCrash(cnx, sql, ts_paramValue);
	}

	public static boolean querryTry(Connection cnx, String sql)
	{

		boolean b = false;

		try
		{
			AzurilSql.querryCrash(cnx, sql);
			b = true;

		} catch (SQLException e)
		{
			b = false;
			System.out.println(" la requette sql : " + sql + " a echoué. ");
		}

		return b;
	}

	public static void envoyerCsvStringVersTable(Connection cnx,
			String tableName, String[] ts_lines) throws SQLException,
			SecurityException, IOException
	{

		SqlRequete requeteSql = new SqlRequete(new LogAzuril("test"),
				tableName, ts_lines, cnx, 1);

		requeteSql.execInsert(cnx);

	}

	public static ArrayList<String> getCol(ResultSet rs) throws SQLException
	{

		ArrayList<String> ls = new ArrayList<String>();

		ResultSetMetaData rsm = rs.getMetaData();

		int columnsNumber = rsm.getColumnCount();

		for (int index = 1; index < columnsNumber + 1; index++)
		{

			String colName = rsm.getColumnName(index);

			ls.add(colName);
		}

		return ls;
	}

	public static String getXml(ResultSet rs) throws SQLException,
			TransformerException, ParserConfigurationException
	{

		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = factory.newDocumentBuilder();
		Document doc = builder.newDocument();
		Element results = doc.createElement("Results");
		doc.appendChild(results);

		ResultSetMetaData rsmd = rs.getMetaData();
		int colCount = rsmd.getColumnCount();

		while (rs.next())
		{

			Element row = doc.createElement("Row");

			results.appendChild(row);

			for (int i = 1; i <= colCount; i++)
			{

				String columnName = rsmd.getColumnName(i);
				Object value = rs.getObject(i);
				Element node = doc.createElement(columnName);

				String s_value = "";

				try
				{
					s_value = value.toString();

				} catch (NullPointerException ex)
				{
					s_value = "";

				} catch (Exception ex)
				{
					s_value = ex.toString();
				}

				Text txt_value = doc.createTextNode(s_value);

				node.appendChild(txt_value);

				row.appendChild(node);
			}

		}

		DOMSource domSource = new DOMSource(doc);
		TransformerFactory tf = TransformerFactory.newInstance();
		Transformer transformer = tf.newTransformer();
		transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
		transformer.setOutputProperty(OutputKeys.METHOD, "xml");
		transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");

		StringWriter sw = new StringWriter();

		StreamResult sr = new StreamResult(sw);

		transformer.transform(domSource, sr);

		String enteteXml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>";

		StringBuilder sb_r = new StringBuilder(enteteXml);

		// sb_r.append("\n");

		String s_xml = sw.toString();

		sb_r.append(s_xml);

		return sb_r.toString();
	}

	public static String getXmlFromSql(Connection cnx, String sql)
			throws SQLException, TransformerException,
			ParserConfigurationException
	{

		ResultSet rs = getResultSetFromSql(cnx, sql);
		return getXml(rs);
	}

	public static String getXmlFromSql(Connection cnx, String sql,
			String[] paramValue) throws SQLException, TransformerException,
			ParserConfigurationException
	{

		ResultSet rs = getResultSetFromSql(cnx, sql, paramValue);
		return getXml(rs);
	}

	public static String getXmlFileFromSql(Connection cnx, String sql,
			Path path_file) throws SQLException, TransformerException,
			ParserConfigurationException, IOException
	{

		ResultSet rs = getResultSetFromSql(cnx, sql);
		String xml = getXml(rs);
		String s_AbsolutePath = AzurilTools.writeAllLines_V2(xml, path_file);

		System.out.println(" path_file.toString() : ");
		System.out.println(path_file.toString());

		return s_AbsolutePath;
	}

}
