package com.azuril.util;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

public class SqlRequeteV1
{

	private String _table = "";
	private String[] _ts_colonnes;
	private String[] _ts_lignes;

	private String _defaultValue = "#NA";

	private boolean _is_Escape = false;

	private Logger _logger;
	private int _premierreColonne;

	public String getDefaultValue()
	{
		return _defaultValue;
	}

	public void setDefaultValue(String _defaultValue)
	{
		this._defaultValue = _defaultValue;
	}

	public boolean getIs_Escape()
	{
		return _is_Escape;
	}

	public void setIs_Escape(boolean _is_Escape)
	{
		this._is_Escape = _is_Escape;
	}

	public SqlRequeteV1(Logger logger, String tableName, String[] ts_colonnes,
			String[] ts_lignes)
	{

		this._logger = logger;
		this._table = tableName;
		this._ts_colonnes = ts_colonnes;
		this._ts_lignes = ts_lignes;

	}

	public SqlRequeteV1(Logger logger, String tableName, String[] ts_lines,
			Connection cnx, int premierreColonne) throws SQLException
	{

		this._logger = logger;
		this._table = tableName;
		this._ts_lignes = ts_lines;
		this._premierreColonne = premierreColonne;

		String sql_getCol = "SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS  WHERE TABLE_NAME = '"
				+ tableName + "'";

		ArrayList<String> ls = AzurilSql.getListCsvFromSql(cnx, sql_getCol);

		this._ts_colonnes = new AzurilTools<String>().getTableStringFromList(ls);

	}

	public SqlRequeteV1(Logger logger, String tableName, String[] ts_lines,
			Connection cnx) throws SQLException
	{
		this(logger, tableName, ts_lines, cnx, 0);
	}

	private String getEntete()
	{
		StringBuilder entete = new StringBuilder();
		entete.append("INSERT INTO " + this._table + " ( ");

		entete.append(_ts_colonnes[_premierreColonne]);

		int i01Max = _ts_colonnes.length;

		for (int i01 = _premierreColonne+1; i01 < i01Max; i01++)
		{
			entete.append(", " + _ts_colonnes[i01]);
		}

		entete.append(" ) VALUES ( ");
		return entete.toString();
	}

	private String[] restructureLine(int nb_colonnes, int ligne)
	{
		String[] val_colonnes_avant = _ts_lignes[ligne].split(";");

		String[] val_colonnes_apres = new String[nb_colonnes];

		for (int i01 = _premierreColonne; i01 < nb_colonnes; i01++)
		{

			if (i01 < val_colonnes_avant.length)
			{

				String s = "";

				s = val_colonnes_avant[i01];

				// if (is_Escape)
				// {
				// s = System.Security.SecurityElement.Escape(s);
				// }

				s = "'" + s.trim() + "'";

				if (s == "'-'")
				{
					s = "'" + this.getDefaultValue() + "'";
				} else if (s == "''")
				{
					s = "'" + this.getDefaultValue() + "'";
				}

				val_colonnes_apres[i01] = s;
			} else
			{
				val_colonnes_apres[i01] = "NULL"; // // on remplis les
													// collonnes
													// non-rensegnier avec
													// '' , suivant
			}

		}

		return val_colonnes_apres;
	}

	public void execInsert(Connection myConnexion) throws SQLException
	{
		// // rend un String sql permetant d'inserer les donnees

		// // phase 1 : on cree le String entete
		String entete = getEntete();

		// // phase 2 : on cree le 1 du corp :

		int ligneMax = _ts_lignes.length;

		for (int ligne = 0; ligne < ligneMax; ligne = ligne + 100)
		{
			StringBuilder sb_sql = new StringBuilder();

			// // on cree la tete

			sb_sql.append(entete);

			// // on restructure la ligne pour correspondre aux colones ,
			// 1er

			String[] val_colonnes02 = this.restructureLine(_ts_colonnes.length,
					ligne);

			// // on remplis la ligne avec ce que l'on a mis dans le tableau
			// , 1er

			sb_sql.append(val_colonnes02[_premierreColonne].trim());

			for (int i02 = _premierreColonne+1; i02 < val_colonnes02.length; i02++)
			{
				sb_sql.append(", " + val_colonnes02[i02].trim());
			}

			for (int ligne02 = ligne + 1; (ligne02 < (ligne + 100))
					&& (ligne02 < ligneMax); ligne02++)
			{

				// / on restructure la ligne pour correspondre aux colones ,
				// suivant

				String[] val_colonnes04 = restructureLine(_ts_colonnes.length,
						ligne02);

				// // on remplis la ligne avec ce que l'on a mis dans le
				// tableau , suivant

				sb_sql.append("), ("); // // on separe les lignes

				sb_sql.append(val_colonnes04[_premierreColonne].trim()); // // on commence
															// la ligne

				for (int i02 = _premierreColonne+1; i02 < val_colonnes04.length; i02++)
				{
					sb_sql.append(", " + val_colonnes04[i02].trim());

					// / on remplis la ligne avec ce que l'on a mis dans le
					// tableau , suivant

				}

			}

			sb_sql.append(");");

			this.printLn(" ligneMax : " + ligneMax + " ligne : " + ligne + " ");

			AzurilSql.querryCrash(myConnexion, sb_sql.toString());

		}

	}

	private void printLn(String msg)
	{
		this._logger.log(Level.INFO, msg);
	}

	public StringBuilder get_sql_Insert(Boolean debugmode)
	{
		// // rend un String sql permetant d'inserer les donnees

		StringBuilder sb_r = new StringBuilder();

		// // phase 1 : on cree le String entete
		String entete = getEntete();

		// // phase 2 : on cree le 1 du corp :

		int ligneMax = _ts_lignes.length;

		for (int ligne = 0; ligne < ligneMax; ligne = ligne + 100)
		{
			StringBuilder sb_sql = new StringBuilder();

			// // on cree la tete

			sb_sql.append(entete);

			// // on restructure la ligne pour correspondre aux colones ,
			// 1er

			String[] val_colonnes02 = restructureLine(_ts_colonnes.length,
					ligne);

			// // on remplis la ligne avec ce que l'on a mis dans le tableau
			// , 1er

			sb_sql.append(val_colonnes02[0].trim());

			for (int i02 = 1; i02 < val_colonnes02.length; i02++)
			{
				sb_sql.append(", " + val_colonnes02[i02].trim());
			}

			for (int ligne02 = ligne + 1; (ligne02 < (ligne + 100))
					&& (ligne02 < ligneMax); ligne02++)
			{

				// / on restructure la ligne pour correspondre aux colones ,
				// suivant

				String[] val_colonnes04 = restructureLine(_ts_colonnes.length,
						ligne02);

				// // on remplis la ligne avec ce que l'on a mis dans le
				// tableau , suivant

				sb_sql.append("), ("); // // on separe les lignes

				sb_sql.append(val_colonnes04[0].trim()); // // on commence
															// la ligne

				for (int i02 = 1; i02 < val_colonnes04.length; i02++)
				{
					sb_sql.append(", " + val_colonnes04[i02].trim());

					// on remplis la ligne avec ce que l'on a mis dans le
					// tableau , suivant

				}

			}

			sb_sql.append(");");

			this.printLn(" ligneMax : " + ligneMax + " ligne : " + ligne + " ");

			sb_r.append(sb_sql);

		}

		return sb_r;
	}

	@Override
	public String toString()
	{
		StringBuilder builder = new StringBuilder();

		builder.append(this.get_sql_Insert(false));

		return builder.toString();
	}

}
