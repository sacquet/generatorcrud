package com.azuril.util;

import java.util.regex.*;

/**
 * 
 * @author Felix ROBICHON
 *
 *         cree le 2016-02-03
 * 
 *         modifier le 2016-02-03
 *
 *         Regex
 * 
 */
public class Regex
{
	private Pattern pattern;

	public Regex(String patern)
	{
		this.pattern = Pattern.compile(patern);
	}

	public boolean isIn(String s)
	{
		boolean b = false;
		Matcher m = pattern.matcher(s);

		while (m.find())
		{
			b = true;
		}

		return b;
	}
	
	public static String getAlfanumeric(String s)
	{
		return s.replaceAll("[^A-Za-z0-9]", "");
	}
	
}
