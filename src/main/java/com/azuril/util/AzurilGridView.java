
package com.azuril.util;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * 
 * @author Felix ROBICHON
 *
 *         cree le 2016-03-23
 * 
 *         modifier le 2016-03-24
 *
 *         genere le code html d'une gridView
 *
 */
public class AzurilGridView
{

	private String _pageCible;

	private String[] _motAction;

	private boolean _isAdd;

	private boolean _isUpdate;

	private String _idSelected;

	public AzurilGridView()
	{

		_pageCible = "";
		_motAction = new String[0];
		_isAdd = false;
		_isUpdate = false;
		_idSelected = "0";

	}

	public AzurilGridView(String pageCible, String[] motAction, boolean isAdd)
	{

		_pageCible = pageCible;
		_motAction = motAction;
		_isAdd = isAdd;
		_isUpdate = false;
		_idSelected = "0";

	}

	public AzurilGridView(String pageCible, String[] motAction, boolean isAdd,
			boolean isUpdate, String idSelected)
	{

		_pageCible = pageCible;
		_motAction = motAction;
		_isAdd = isAdd;
		_isUpdate = isUpdate;
		_idSelected = idSelected;

	}

	private StringBuilder getEntete(ArrayList<String> ls_col)
	{

		StringBuilder sb = new StringBuilder();

		for (int i = 0; i < _motAction.length; i++)
		{
			sb.append("<th scope=\"col\"></th>");
		}

		for (int i = 0; i < ls_col.size(); i++)
		{
			String s = ls_col.get(i);

			sb.append("<th scope=\"col\">" + s + "</th>");
		}

		return sb;
	}

	public static StringBuilder getHtmlUpdate_V1(ArrayList<String> ls_col,String pageCible)
	{

		StringBuilder sb = new StringBuilder();

		sb.append("<form class=\"AzurilGridView_form\" method=\"post\" action=\"");
		sb.append(pageCible);
		sb.append("?AzurilGridView_action=update\" >");

		for (int i = 0; i < ls_col.size(); i++)
		{

			String s = ls_col.get(i);

			sb.append("<label for=\"" + s + "\" >" + s + "</label><br />\n");

			sb.append("<input id=\"" + s + "\" name=\"" + s + "\" ");

			sb.append(" value=\"\"");

			sb.append("\" type=\"text\" >\n");

			// sb.append(ls_lineValue[i]);

			sb.append("</input><br />\n");

		}

		sb.append("<br />\n");
		sb.append("<input type=\"submit\" value=\"update\" />");
		sb.append("<br />\n");
		sb.append("</form>");

		return sb;
	}

	public static StringBuilder getHtmlUpdate(ArrayList<String> ls_col,
			String line, String _pageCible)
	{

		StringBuilder sb = new StringBuilder();

		sb.append("<form class=\"AzurilGridView_form\" method=\"post\" action=\"");
		sb.append(_pageCible);
		sb.append("?AzurilGridView_action=update\" >");

		String[] ts_line = line.split(";");

		for (int i = 0; i < ls_col.size(); i++)
		{

			String s = ls_col.get(i);

			sb.append("<label for=\"" + s + "\" >" + s + "</label><br />\n");

			sb.append("<input id=\"" + s + "\" name=\"" + s + "\" ");

			sb.append(" value=\"" + ts_line[i] + "\"");

			sb.append("\" type=\"text\" >\n");

			// sb.append(ls_lineValue[i]);

			sb.append("</input><br />\n");

		}

		sb.append("<br />\n");
		sb.append("<input type=\"submit\" value=\"update\" />");
		sb.append("<br />\n");
		sb.append("</form>");

		return sb;
	}

	private StringBuilder getHtmlUpdate(ArrayList<String> ls_col, String line)
	{
		return getHtmlUpdate(ls_col, line, _pageCible);
	}

	private StringBuilder getHtmlInsert(ArrayList<String> ls_col)
	{

		StringBuilder sb = new StringBuilder();

		sb.append("<form class=\"AzurilGridView_form\" method=\"post\" action=\"");
		sb.append(_pageCible);
		sb.append("?AzurilGridView_action=insert\" >");

		for (int i = 1; i < ls_col.size(); i++)
		{

			String s = ls_col.get(i);

			sb.append("<label for=\"" + s + "\" >" + s + "</label><br />\n");

			sb.append("<input id=\"" + s + "\" name=\"" + s);

			sb.append("\" type=\"text\" ></input><br />\n");

		}

		sb.append("<br />\n");
		sb.append("<input type=\"submit\" value=\"insert\" />");
		sb.append("<br />\n");
		sb.append("</form>");

		return sb;
	}

	private StringBuilder getContenu(String[] ts_csv)
	{

		StringBuilder sb = new StringBuilder();

		int i01_min = 0;

		for (int i01 = i01_min; i01 < ts_csv.length; i01++)
		{

			String[] ts_ligne = ts_csv[i01].split(";");

			sb.append("\n");

			sb.append("<tr class=\"AzurilGridView_");

			// si paire
			if (i01 % 2 == 0)
			{
				sb.append("paire");

			} else
			{
				sb.append("impaire");
			}

			sb.append("\">");

			for (int i = 0; i < _motAction.length; i++)
			{
				String typeAction = _motAction[i];

				sb.append("<td><a href=\"");
				sb.append(_pageCible);
				sb.append("?AzurilGridView_id=");
				sb.append(ts_ligne[0]);
				sb.append("&AzurilGridView_action=");
				sb.append(typeAction);
				sb.append("\">");
				sb.append(typeAction);
				sb.append("</a></td>");

				sb.append("\n");
			}

			for (int i02 = 0; i02 < ts_ligne.length; i02++)
			{
				sb.append("<td>");
				sb.append(ts_ligne[i02]);
				sb.append("</td>");

				sb.append("\n");

			}

			sb.append("</tr>");

			sb.append("\n");

		}

		return sb;
	}

	public StringBuilder getStringBuilderAzurilGridView(Connection cnx,
			String sql) throws SQLException
	{

		StringBuilder sb = new StringBuilder();

		ResultSet rs = AzurilSql.getResultSetFromSql(cnx, sql);

		ArrayList<String> ls_col = new ArrayList<String>();

		ls_col = AzurilSql.getCol(rs);

		ArrayList<String> ls_csv = AzurilSql.getListCsvFromSql(rs);

		if (_isUpdate)
		{

			String line = "";

			for (String currentLine : ls_csv)
			{
				if (_idSelected.equals(currentLine.split(";")[0]))
				{
					line = currentLine;
					break;
				}
			}

			if (!line.isEmpty())
			{
				sb.append(getHtmlUpdate(ls_col, line));
			}

		}

		sb.append("\n");

		sb.append("<table id=\"AzurilGridView_table\" class=\"AzurilGridView_table\"\n");
		sb.append("border=\"0\" cellpadding=\"4\" cellspacing=\"0\">\n");
		sb.append("<tbody>\n");
		sb.append("<tr class=\"AzurilGridView_entete\">\n");

		sb.append("\n");

		sb.append(getEntete(ls_col));

		sb.append("</tr> \n");

		String[] ts_csv = new AzurilTools<String>().getTableStringFromList(ls_csv);

		sb.append(getContenu(ts_csv));

		sb.append(" </tbody> </table> \n");

		if (_isAdd)
		{
			sb.append(getHtmlInsert(ls_col));
		}

		sb.append("\n");

		return sb;
	}

	@SuppressWarnings("unused")
	private String exec(Connection cnx, String sql, boolean pageEntierre,
			boolean isAdd)
	{

		// @SuppressWarnings("unused")
		// String s_seconde = "";

		// String s_seconde = Outil.seconde();

		// /======================================================================
		// /
		// / debut Main_X3_PRODULYS_05_STO_NEG
		// /
		// /======================================================================

		StringBuilder sb = new StringBuilder();

		try
		{

			sb.append(getStringBuilderAzurilGridView(cnx, sql));

		} catch (Exception ex)
		{

			StringBuilder err = new StringBuilder();

			err.append("<hr />");

			err.append(ex.toString());

			err.append("<hr />");

			// <hr /><pre class=\"AzurilGridView_console\">
			// <span class=\"AzurilGridView_sql\" >
			// dernierre modif a 10h44 le 2015-05-21 <br />
			// </span>
			//
			// <span class=\"AzurilGridView_sql\" >
			// myConnection.ConnectionString : <br />
			// " + myConnection.ConnectionString + "<br />
			// </span>
			//
			// <span class=\"AzurilGridView_sql\" >
			// sql : <br />
			// " + sql + "<br />
			// </span>
			//
			// <span class=\"AzurilGridView_erreur\" >
			// erreur : <br />
			// " + ex.ToString() + "<br />
			// </span>
			//
			// </pre><hr />";

			AzurilTools.printLn(err.toString());

			sb.insert(0, err);

			sb.append(err);

		}

		return sb.toString();
	}

	public void Dispose()
	{

	}

}
