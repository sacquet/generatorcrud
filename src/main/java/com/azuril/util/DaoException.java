package com.azuril.util;


public class DaoException extends Exception
{

	private static final long serialVersionUID = 1L;

	public DaoException(String message, Exception cause)
	{
		super(message, cause);
	}
	
}
