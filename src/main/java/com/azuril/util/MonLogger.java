
package com.azuril.util;

import java.util.logging.ConsoleHandler;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.Logger;

public class MonLogger
{

	private static String _tag = "INIT";

	private static Logger _logger = null;

	static
	{
		
		String tag = System.getProperty("program.name");
		
		if(tag != null)
		{
			_tag = tag;
		}
		
		_logger = Logger.getLogger(_tag);
		_logger.setUseParentHandlers(false);
		_logger.setLevel(Level.ALL);
		Handler handler = new ConsoleHandler();
		_logger.addHandler(handler);
	}

	public static Logger getLogger()
	{

		_logger = Logger.getLogger(_tag);
		return _logger;
	}

	public static String getTag()
	{

		return _tag;
	}

	public static void setTag(String tag)
	{

		MonLogger._tag = tag;
	}

}
