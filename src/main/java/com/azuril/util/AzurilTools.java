
package com.azuril.util;

import java.io.*;
import java.util.*;
import java.nio.charset.*;
import java.nio.file.*;

import javax.swing.table.DefaultTableModel;

/**
 * 
 * @author Felix ROBICHON
 *
 *         cree le 2016-01-25
 * 
 *         modifier le 2016-04-02
 *
 * gnap 
 *
 *         ensemble d'outil basique
 *
 * @param <T>
 */
public class AzurilTools<T>
{

	public AzurilTools()
	{

	}

	public static final String emptyString = "";

	public static final String newLine = System.getProperty("line.separator");

	public static void bug()
	{

		String[] ts = new String[1];

		String bug = ts[2];

		System.out.println(bug);
	}

	/**
	 * 
	 * rend un nombre entier aleatoire
	 * 
	 * @param nombreMaximum
	 * @return
	 */
	public static int getNombreAleatoire(int nombreMaximum)
	{

		int nombreAeatoireResultat = 0;

		Random randomInstance = new Random();

		nombreAeatoireResultat = randomInstance.nextInt(nombreMaximum);

		return nombreAeatoireResultat;
	}

	public static void sleepMs(int ms)
	{

		try
		{
			Thread.sleep(ms);

		} catch (InterruptedException e)
		{
			e.printStackTrace();
		}

	}

	/**
	 * 
	 * rend une table avec les meme valeur a partir d'une liste
	 * 
	 * @param listInt
	 * @return
	 */
	public static int[] getTableIntFromListInt(List<Integer> listInt)
	{

		int[] tableInt = new int[listInt.size()];

		for (int i = 0; i < tableInt.length; i++)
		{
			tableInt[i] = listInt.get(i);
		}

		return tableInt;
	}

	public String[] getTableStringFromList(List<T> listT)
	{

		int iMax = listT.size();

		String[] r = new String[iMax];

		for (int i = 0; i < iMax; i++)
		{
			r[i] = listT.get(i).toString();
		}

		return r;
	}

	public String getStringFromList(List<T> listT)
	{

		StringBuilder sb = new StringBuilder();

		for (int i = 0; i < listT.size(); i++)
		{
			sb.append(listT.get(i));
			sb.append(AzurilTools.newLine);
		}

		return sb.toString();
	}

	/**
	 * 
	 * rend une table avec les meme valeur a partir d'une liste
	 * 
	 * @param listT
	 * @return
	 */
	public T[] getTableFromList(List<T> listT)
	{

		Object[] t_obj = listT.toArray();

		@SuppressWarnings("unchecked")
		T[] r = (T[]) t_obj;

		return r;
	}

	/**
	 * 
	 * rend un tableau rempis d'entier dans le desordre
	 * 
	 * @param size
	 * @return
	 */
	public static int[] getTableauDeNombreAleatoire_v1(int size)
	{

		List<Integer> li_tableDesNombres = new ArrayList<Integer>();

		Random randomInstance = new Random();

		for (int i = 0; i < size; i++)
		{
			li_tableDesNombres.add(i);
		}

		List<Integer> li_nombresMelange = new ArrayList<Integer>();

		for (int i = li_tableDesNombres.size(); i >= 1; i--)
		{

			int nb_aleatoire = randomInstance.nextInt(i);

			int nb = li_tableDesNombres.get(nb_aleatoire);

			li_tableDesNombres.remove(new Integer(nb));

			li_nombresMelange.add(nb);

		}

		int[] tableResultat = getTableIntFromListInt(li_nombresMelange);

		return tableResultat;
	}

	/**
	 * 
	 * rend un tableau rempis d'entier dans le desordre
	 * 
	 * @param size
	 * @return
	 */
	public static int[] getTableauDeNombreAleatoire(int size)
	{

		List<Integer> li_tableDesNombres = new ArrayList<Integer>();

		for (int i = 0; i < size; i++)
		{
			li_tableDesNombres.add(i);
		}

		Collections.shuffle(li_tableDesNombres);

		int[] tableResultat = getTableIntFromListInt(li_tableDesNombres);

		return tableResultat;
	}

	/**
	 * 
	 * rend une liste avec les meme valeur a partir d'une table
	 * 
	 * @param tableDeT
	 * @return
	 */
	public ArrayList<T> getListFromTable(T[] tableDeT)
	{

		ArrayList<T> listeDeT = new ArrayList<T>();

		for (int i = 0; i < tableDeT.length; i++)
		{
			listeDeT.add(tableDeT[i]);
		}

		return listeDeT;
	}

	public Vector<T> getVectorFromTable(T[] tableDeT)
	{

		Vector<T> listeDeT = new Vector<T>();

		for (int i = 0; i < tableDeT.length; i++)
		{
			listeDeT.add(tableDeT[i]);
		}

		return listeDeT;
	}

	public static boolean isBlanc(String s)
	{

		if (s == null)
		{
			return true;
		}

		s = s.trim();

		return s.isEmpty();
	}

	public static void notImplemented()
	{

		throw new UnsupportedOperationException(" Erreur : cette fonction n'a pas été implémenté. ");
	}

	// / a tester
	public static List<String> readAllLines(String path_file) throws IOException
	{

		Charset cs = StandardCharsets.UTF_8;
		Path chemin = Paths.get(path_file);
		List<String> r = Files.readAllLines(chemin, cs);
		return r;
	}

	public static void writeAllLines(String s, String path_file) throws IOException
	{

		try
		{
			FileWriter fw = new FileWriter(path_file);
			fw.write(s);
			fw.close();

		} catch (IOException ex)
		{
			String err = " Erreur a l'ecriture du fichier " + path_file + " ";
			System.out.println(err);
			ex.printStackTrace();
			throw new IOException(err, ex);
		}

	}

	public static boolean creeFichier(String nomFichier)
	{

		boolean b;

		try
		{
			File test1 = new File(nomFichier);
			test1.createNewFile();
			b = true;

		} catch (IOException e)
		{
			b = false;
		}

		return b;
	}

	public static <T extends Serializable> void serialiserBinaire(String nomFichier, List<T> obj) throws IOException
	{

		creeFichier(nomFichier);

		ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(nomFichier));

		oos.writeObject(obj);
		oos.flush();

		oos.close();

	}

	@SuppressWarnings("unchecked")
	public T deSerialiserBinaire(String nomFichier) throws IOException, ClassNotFoundException
	{

		ObjectInputStream oos = new ObjectInputStream(new FileInputStream(nomFichier));
		T r = (T) oos.readObject();
		oos.close();
		return r;
	}

	public static DefaultTableModel getTableModelFromCsv(String[] titresColonnes, ArrayList<String> csv)
	{

		Vector<String> vs_column = new AzurilTools<String>().getVectorFromTable(titresColonnes);

		DefaultTableModel tm = new DefaultTableModel(0, vs_column.size());

		tm.setColumnIdentifiers(vs_column);

		for (String line : csv)
		{
			Vector<String> vs = new Vector<String>();

			String[] ts_celules = line.split(";", 0);

			for (int i = 0; i < titresColonnes.length; i++)
			{
				vs.add(ts_celules[i]);
			}

			tm.addRow(vs);
		}

		return tm;
	}

	public static void writeAllLines(ArrayList<String> csvContenuTable, Path file) throws IOException
	{

		String path_file = file.toString();

		try
		{
			FileWriter fw = new FileWriter(path_file);

			for (String string : csvContenuTable)
			{
				fw.write(string);
			}

			fw.close();

		} catch (IOException ex)
		{

			String err = " Erreur a l'ecriture du fichier " + path_file + " ";
			System.out.println(err);
			ex.printStackTrace();

			throw new IOException(err, ex);
		}

	}

	public String toString(List<T> tableObj)
	{

		StringBuilder sb = new StringBuilder();

		for (Object object : tableObj)
		{
			sb.append(object);
		}

		return sb.toString();
	}

	public static String writeAllLines_V1(String s, Path file) throws IOException
	{

		String path_file = file.toString();

		try (FileWriter fw = new FileWriter(path_file))
		{
			File file_out_01 = new File(path_file);
			file_out_01.delete();

		} catch (Exception ex)
		{
			// / volontairement vide
		}

		try (FileWriter fw = new FileWriter(path_file))
		{
			fw.write(s);

		} catch (IOException ex)
		{
			String err = " Erreur a l'ecriture du fichier " + path_file + " ";
			System.out.println(err);
			ex.printStackTrace();

			throw new IOException(err, ex);
		}

		File file_out = new File(path_file);

		String s_AbsolutePath = file_out.getAbsolutePath();

		return s_AbsolutePath;
	}

	public static String writeAllLines_V2(String s, Path file) throws IOException
	{

		String path_file = file.toString();

		try
		{
			new File(path_file).delete();

		} catch (Exception ex)
		{
			// / volontairement vide
		}

		OutputStreamWriter char_output = new OutputStreamWriter(new FileOutputStream(path_file),
				Charset.forName("UTF-8").newEncoder());

		char_output.write(s);

		char_output.close();

		String s_AbsolutePath = path_file;

		return s_AbsolutePath;
	}

	public static void printLn(String s)
	{

		System.out.println(s);
	}

	public static Set<String> distinct(List<String> gasList)
	{

		// / This HashSet constructor identifies duplicates by invoking the
		// elements' equals() methods.
		Set<String> uniqueGas = new HashSet<String>(gasList);

		return uniqueGas;
	}

	public static boolean createFolder(File theDir)
	{

		boolean result = false;

		// if the directory does not exist, create it
		if (!theDir.exists())
		{

			theDir.mkdir();
			result = true;

		}

		return result;
	}

	public static boolean createFolder(String pathFolder)
	{

		return createFolder(new File(pathFolder));
	}

}
