package com.azuril.util;

import java.io.IOException;
import java.util.logging.ConsoleHandler;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

public class LogAzuril extends Logger
{

	/**
	 * 
	 * @author Felix ROBICHON
	 *
	 *         cree le 2016-02-01
	 * 
	 *         modifier le 2016-02-04
	 *
	 *         ensemble d'outil basique
	 *
	 * @param isLogXML
	 * @param isLogTXT
	 * @throws IOException
	 * @throws SecurityException
	 */
	public LogAzuril(String programName, Level ConsolLvl, Level txtLvl) throws SecurityException, IOException
	{
		super(programName,null);
		
		super.setLevel(txtLvl);

		ConsoleHandler ch = new ConsoleHandler();
		ch.setLevel(ConsolLvl); // info ou plus.

		// if (isLogXML)
		// {
		// FileHandler fh_01 = new FileHandler("mesLogs.log.xml");
		// fh_01.setLevel(Level.SEVERE);
		// monLog.addHandler(fh_01);
		// }
		//
		// if (isLogTXT)
		// {
		
		FileHandler fh_02 = new FileHandler("mesLogs.log.txt");
		fh_02.setLevel(txtLvl);
		fh_02.setFormatter(new SimpleFormatter());
		super.addHandler(fh_02);
		
		// }

		this.log(Level.INFO, " debut log ");
		
		this.log(Level.FINE, " txt Lvl : "+txtLvl);

	}

	public LogAzuril(String programName) throws SecurityException, IOException
	{
		this(programName,Level.INFO,Level.SEVERE);
	}
	
	public void log(Level level, String msg)
	{

		super.log(level, msg);

		if (level.equals(Level.SEVERE) || level.equals(Level.WARNING))
		{
			System.err.println(msg);
			
		} else
		{
			System.out.println(msg);
		}

	}

}
